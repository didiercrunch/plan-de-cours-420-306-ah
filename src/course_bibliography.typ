#let course-bibliography = [
// Bellow is a little hack to display bibliography 
// whitout using them in the text.  Indeed, we use
// them in a text but we set the size to 0.
#text(size: 0em)[
    @csharp_official_doc
    @programming_cshap10
    @csharp_wikipedia
]



#bibliography("bibliography.yaml")


]