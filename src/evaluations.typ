#let evaluations = [

== Résumé synthèse de l'évaluation

=== Évaluation formative :
À l'occasion, l'enseignant fournira des exercices à faire en classe ou des devoirs à compléter à la maison qui permettront à l'élève de vérifier sa compréhension de la nouvelle matière présentée et de se familiariser avec le type de questions auxquelles il doit être en mesure de répondre.


=== Évaluation sommative adaptée


#table(
  columns: (auto, 1fr),
  inset: 10pt,
  align: horizon,
  [Mini Tests], [15%],
  [Travaille Pratique 1],[15%],
  [Travaille Pratique 2],[25%],
  [Travaille Pratique 3],[25%],
  [Test Final],[20%]  
)

==== Notes relatives aux évaluations

- La note de passage est de 60%.
- Les travaux doivent être remis à l'échéance fixée.
- En cas de plagiat, la note zéro sera attribuée pour l'évaluation concernée.
- Vous avez la responsabilité de conserver vos évaluations et vous devrez présenter celles-ci lors d'une demande de révision de note.

]