#import "frontpage.typ": frontpage
#import "generalites.typ": generalites
#import "evaluations.typ": evaluations
#import "deroulement_du_cours/etape_1.typ": etape1
#import "deroulement_du_cours/etape_2.typ": etape2
#import "deroulement_du_cours/etape_3.typ": etape3
#import "deroulement_du_cours/etape_4.typ": etape4
#import "deroulement_du_cours/etape_5.typ": etape5
#import "course_bibliography.typ": course-bibliography
#let params = toml("main.toml")

#set page(
  footer: [
    #set align(center)
    #set text(8pt)
    Page #counter(page).display(
      "1 de 1",
      both: true,
    ) #linebreak()
    #text(size: 7pt, font: "Fira Code")[Dernière mise-à-jour: #params.at(default: "", "lastUpdated") | Identification: #params.at(default: "", "githash")]
  ]
)


#frontpage

#pagebreak()


#generalites

== Déroulement du cours


#etape1

#etape2

#etape3

#etape4

#etape5

#evaluations

#course-bibliography