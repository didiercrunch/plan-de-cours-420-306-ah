#let _ponderation = link("https://www.cegepsquebec.ca/nos-cegeps/presentation/systeme-scolaire-quebecois/grille-de-cours-et-ponderation/")[Pondération]

#let frontpage = [ 
#align(center)[
    #image("logo-ahuntsic.png", width: 33%),

    #text(size: 20pt)[Plan De Cours]

    #text(size: 12pt)[Automn 2023]
]

#table(
  columns: (auto, 1fr),
  inset: 25pt,
  align: horizon,
  [Titre du cours],[*Programmation Web côté serveur III*],
  [Code],[*420-306-AH*],
  _ponderation, text(weight: "bold", font: "FreeMono")[2-3-2],
  [Compétences visées], list(
        [*AF57 - Effectuer le développement d'applications Web transactionnelles*],
        [*00Q6 - Exploiter les principes de la programmation orientée objet*]
    ),
  [Programme], [*Développement de sites Web transactionnels*],
  [Enseignant], [
    - *Didier Amyot*
    - #link("mailto:didier.amyot@collegeahuntsic.qc.ca")[didier.amyot\@collegeahuntsic.qc.ca]
    ],
  [Département], [*Informatique*]
)
]

