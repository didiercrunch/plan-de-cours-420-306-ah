#let generalites = [
== Présentation générale

=== Objectif-standard visé
Le programme de AEC en Développement d'applications mobiles (LEA.CB) ainsi que les compétences ministérielles peuvent être consultées sur le site Web du Collège.


=== Présentation du cours
Ce cours du 4#super[e] bloc est le dernier cours d'une série de trois cours portant sur le développement d'applications Web côté serveur. Il permet d'introduire d'autres technologies du développement d'application Web transactionnelles. Il intègre les notions de programmation orientée objet déjà acquises par l'étudiant. Il permet à l'étudiant de développer, dans un nouvel environnement, une application Web client-serveur fonctionnant dans un environnement ASP.NET utilisant Entity Framework.

=== Objectifs terminaux du cours 
Les objectifs intermédiaires de ce cours sont de programmer une application utilisant les concepts orientés objet du langage ASP.NET, de programmer une interface utilisateur pour une application répondant aux besoins de l'utilisateur final, de programmer une application utilisant des composantes de base et interrogeant une base de données avec Entity Framework et de valider le bon fonctionnement de l'application.
]