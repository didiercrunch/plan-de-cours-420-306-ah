#let etape3 = [
=== Étape 3: Création d'une application CRUD (Create, Read, Update et Delete) sans Entity FrameWork


==== Objectifs spécifiques

Apprendre les différents aspects spécifiques d'un projet MVC qu'utilise une base de données et implanter un CRUD.

==== Contenu
- Création d'une base de données SQL
- Générer une application MVC  
- Ajouter de contrôleurs 
- Ajouter des modèles de données 
- Générer l'application

=== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques par l'enseignant permettra à l'élève de réaliser les activités d'apprentissage.

=== Activités d'apprentissage

Concevoir un petit projet MVC qu'utilise une base de données et concevoir un CRUD pour cette application.

]