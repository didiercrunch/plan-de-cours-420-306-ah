#let etape4 = [
=== Étape 4: Création d'une application CRUD avec Entity FrameWork (EF)


==== Objectifs spécifiques

Apprendre les différents aspects spécifiques d'un projet MVC qu'utilise une base de données et utiliser Entity Framework pour générer le CRUD de l'application.

==== Contenu
- Linq
- Générer une application MVC utilisant EF Database First 
 
- Générer l'échafaudage 
- Générer l'application 

- Générer une application MVC utilisant EF CodeFirst 


=== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques par l'enseignant permettra à l'élève de réaliser les activités d'apprentissage.

=== Activités d'apprentissage

Concevoir un petit projet MVC qu'utilise une base de données et concevoir un CRUD pour cette application.

]