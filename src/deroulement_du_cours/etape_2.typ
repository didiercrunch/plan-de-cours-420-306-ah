#let etape2 = [
=== Étape 2: Vue d'ensemble du modèle-vue-contrôleur (MVC) et éléments de base


==== Objectifs spécifiques

À l'issue de cette étape, l'étudiant sera en mesure de créer un petit projet utilisant le modèle MVC et utiliser un certain nombre d'éléments de base de MVC. 

==== Contenu
- Les différents dossiers et fichiers du modèle MVC
- Exemples illustratifs de gestion d'une requête  
- Traitement d'une requête
- Les vues fortement typées 
- Syntaxe d'une vue Razor
- ViewBag, ViewData, TempData et Session
- La classe HTMLHelper 
- Utilisation des vues partielles et des pages de disposition


=== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques par l'enseignant permettra à l'élève de réaliser les activités d'apprentissage.

=== Activités d'apprentissage

Concevoir un petit projet MVC et utiliser certains éléments propres à ce type de projet. 

]