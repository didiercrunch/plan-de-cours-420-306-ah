#let etape5 = [
=== Étape 5: Outils de collaboration et de gestion de version (Github)


==== Objectifs spécifiques

Apprendre les différents des outils de collaboration et de gestion de versions


==== Contenu
- Configuration d'un système de gestion de version 

- Commit/checkout/merge, etc. 

 
- Gestion des branches

- Générer l'application 

- Générer une application MVC utilisant EF CodeFirst 


=== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques par l'enseignant permettra à l'élève de réaliser les activités d'apprentissage.

=== Activités d'apprentissage

En classe (théorie et laboratoire), l'étudiant découvre les divers concepts présentés par le professeur afin d'élaborer des stratégies de collaboration et gestion des versions.


]