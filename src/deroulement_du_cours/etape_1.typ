#let _vscode = link("https://code.visualstudio.com")[VsCode]
#let _git = link("https://git-scm.com/")[git]


#let etape1 = [
=== Étape 1: Installation de l'environement de développement et vue d'ensemble du langage C\#

==== Objectifs spécifiques

- Installation de l'environnement de développement  #_vscode
  et du system de gestion de versions #_git. 
 #_git
- Apprendre les rudiments de POO du langage C\#

==== Contenu
- Installation
- Configuration 
- Tests
- Vue d'ensemble du langage C\#
- Types de base 
- Conteneurs 
- Fichiers 
- Programmation orientée objet 

=== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques par l'enseignant permettra à l'élève de réaliser les activités d'apprentissage.

=== Activités d'apprentissage

Installation de l'environnement et écriture de petits programmes en langage C.
]